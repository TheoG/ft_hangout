package com.theogros.ft_hangout.controllers;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.telephony.SmsMessage;

import com.theogros.ft_hangout.R;
import com.theogros.ft_hangout.activities.HomeActivity;
import com.theogros.ft_hangout.models.ContactContract;

public class SmsReceiver extends BroadcastReceiver {
	public static final String ACTION = "android.provider.Telephony.SMS_RECEIVED";

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent != null && intent.getAction() != null && ACTION.compareToIgnoreCase(intent.getAction()) == 0) {

			Bundle bundle = intent.getExtras();

			if (bundle == null) {
				return ;
			}
			Object[] pduArray = (Object[]) bundle.get("pdus");
			if (pduArray == null) {
				return ;
			}
			SmsMessage[] messages = new SmsMessage[pduArray.length];
			for (int i = 0; i < pduArray.length; i++) {
				messages[i] = SmsMessage.createFromPdu((byte[]) pduArray[i], "3gpp");

				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
					CharSequence name = "name";
					String description = "description";

					int importance = NotificationManager.IMPORTANCE_DEFAULT;
					NotificationChannel channel = new NotificationChannel("1", name, importance);
					channel.setDescription(description);
					NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
					if (notificationManager == null) {
						return ;
					}
					notificationManager.createNotificationChannel(channel);
				}

				ContactController controller = new ContactController(context);

				ContactContract contact = new ContactContract();
				contact.setNumber(messages[i].getDisplayOriginatingAddress());
				long ret = controller.conditionalInsertion(contact);
				controller.close();

				if (ret > 0) {
					Intent notificationIntent = new Intent(context, HomeActivity.class);
					notificationIntent .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
					PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);


					NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "1");
					builder.setSmallIcon(R.drawable.ic_account_circle_black_24dp);
					builder.setContentTitle(context.getString(R.string.notification_title));
					builder.setContentText(contact.getNumber() + " " + context.getString(R.string.notification_content));
					builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
					builder.setContentIntent(pendingIntent);
					builder.setAutoCancel(true);

					NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
					notificationManager.notify(1, builder.build());
				}
			}
		}
	}
}