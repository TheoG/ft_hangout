package com.theogros.ft_hangout.controllers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by theog on 19/05/2018.
 */

public class
ContactDbHelper extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 2;
	private static final String DATABASE_NAME = "ContactContract.db";

	protected static final String CONTACT_TABLE_NAME = "contact";
	protected static final String COL_ID = "ID";
	protected static final String COL_FIRST_NAME = "FirstName";
	protected static final String COL_LAST_NAME = "LastName";
	protected static final String COL_NUMBER = "Number";
	protected static final String COL_ADDRESS = "Address";
	protected static final String COL_EMAIL_ADDRESS = "EmailAddress";
	protected static final String COL_HAS_IMAGE = "HasImage";

	private static final String CONTACT_TABLE_CREATE =
			"CREATE TABLE IF NOT EXISTS " + CONTACT_TABLE_NAME + " ("
					+ COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
					+ ContactDbHelper.COL_FIRST_NAME + " TEXT NOT NULL, "
					+ ContactDbHelper.COL_LAST_NAME + ", "
					+ ContactDbHelper.COL_ADDRESS + ", "
					+ ContactDbHelper.COL_EMAIL_ADDRESS + ", "
					+ ContactDbHelper.COL_NUMBER + " TEXT NOT NULL, "
					+ ContactDbHelper.COL_HAS_IMAGE + " INTEGER);";

	ContactDbHelper(Context context) {
		super (context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CONTACT_TABLE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + ContactDbHelper.CONTACT_TABLE_NAME + ";");
		this.onCreate(db);
	}
}
