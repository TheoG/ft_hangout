package com.theogros.ft_hangout.controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


/**
 * Created by theog on 19/05/2018.
 */

public class SettingsController {

	private SQLiteDatabase db;
	private SettingsDbHelper settingsDb;

	public SettingsController(Context context) {
		this.settingsDb = new SettingsDbHelper(context);
		this.open();
	}

	private void open() {
		this.db = this.settingsDb.getWritableDatabase();
		this.settingsDb.onCreate(this.db);
	}

	public String getSettingValue(String field) {
		Cursor cursor = db.query(SettingsDbHelper.SETTINGS_TABLE_NAME, new String[] {
				SettingsDbHelper.COL_ID,
				SettingsDbHelper.COL_SETTING_NAME,
				SettingsDbHelper.COL_SETTING_VALUE}, SettingsDbHelper.COL_SETTING_NAME+"=?", new String[] { field }, null, null, null);
		if (cursor.getCount() > 0) {
			cursor.moveToNext();
		}
		return this.cursorToString(cursor);
	}

	public void updateSetting(String setting, String value) {

		ContentValues values = new ContentValues();

		values.put(SettingsDbHelper.COL_SETTING_VALUE, value);

		this.db.update(SettingsDbHelper.SETTINGS_TABLE_NAME, values, SettingsDbHelper.COL_SETTING_NAME + "=?", new String[] { setting });
	}

	private String cursorToString(Cursor c){
		if (c.getCount() == 0) {
			return null;
		}

		String dbValue = c.getString(2);
		c.close();

		return dbValue;
	}

	public void close() {
		this.db.close();
	}
}
