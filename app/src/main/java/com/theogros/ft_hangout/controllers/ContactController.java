package com.theogros.ft_hangout.controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.theogros.ft_hangout.models.ContactContract;

import java.util.ArrayList;

/**
 * Created by theog on 19/05/2018.
 */

public class ContactController {

	private SQLiteDatabase db;
	private ContactDbHelper contactDb;

	public static final String contactFileNamePattern = "contact_image_";
	public static final String contactFileExtension = ".jpeg";

	public ContactController(Context context) {
		this.contactDb = new ContactDbHelper(context);
		this.open();
	}

	private void open() {
		this.db = this.contactDb.getWritableDatabase();
		this.contactDb.onCreate(this.db);
	}

	public void close() {
		this.db.close();
	}

	public long insertContact(ContactContract contactContract) {
		ContentValues values = new ContentValues();

		values.put(ContactDbHelper.COL_FIRST_NAME, contactContract.getFirstName());
		values.put(ContactDbHelper.COL_LAST_NAME, contactContract.getLastName());
		values.put(ContactDbHelper.COL_ADDRESS, contactContract.getAddress());
		values.put(ContactDbHelper.COL_EMAIL_ADDRESS, contactContract.getEmailAddress());
		values.put(ContactDbHelper.COL_NUMBER, contactContract.getNumber());
		values.put(ContactDbHelper.COL_HAS_IMAGE, contactContract.hasImage());

		return this.db.insert(ContactDbHelper.CONTACT_TABLE_NAME, null, values);
	}

	public long conditionalInsertion(ContactContract contactContract) {

		Cursor cursor = db.query(ContactDbHelper.CONTACT_TABLE_NAME, new String[] {
				ContactDbHelper.COL_ID,
				ContactDbHelper.COL_NUMBER}, ContactDbHelper.COL_NUMBER+"=?", new String[] { contactContract.getNumber() }, null, null, null);
		if (cursor.getCount() == 0) {

			ContentValues values = new ContentValues();

			values.put(ContactDbHelper.COL_FIRST_NAME, contactContract.getNumber());
			values.put(ContactDbHelper.COL_NUMBER, contactContract.getNumber());

			return this.db.insert(ContactDbHelper.CONTACT_TABLE_NAME, null, values);

		}

		cursor.close();


		return 0;
	}

	public ArrayList<ContactContract> getAllContacts() {
		ArrayList<ContactContract> contactContracts = new ArrayList<>();
		Cursor cursor = db.query(ContactDbHelper.CONTACT_TABLE_NAME, new String[] {
				ContactDbHelper.COL_ID,
						ContactDbHelper.COL_FIRST_NAME,
						ContactDbHelper.COL_LAST_NAME,
						ContactDbHelper.COL_ADDRESS,
						ContactDbHelper.COL_EMAIL_ADDRESS,
						ContactDbHelper.COL_NUMBER,
						ContactDbHelper.COL_HAS_IMAGE},
				null, null, null, null, null);

		if (cursor.getCount() > 0) {
			while (cursor.moveToNext()) {
				contactContracts.add(this.cursorToContact(cursor));
			}
		}
		return contactContracts;
	}


	public ContactContract getContactByID(int id) {
		Cursor cursor = db.query(ContactDbHelper.CONTACT_TABLE_NAME, new String[] {
				ContactDbHelper.COL_ID,
				ContactDbHelper.COL_FIRST_NAME,
				ContactDbHelper.COL_LAST_NAME,
				ContactDbHelper.COL_ADDRESS,
				ContactDbHelper.COL_EMAIL_ADDRESS,
				ContactDbHelper.COL_NUMBER,
				ContactDbHelper.COL_HAS_IMAGE}, ContactDbHelper.COL_ID+"=?", new String[] { Integer.toString(id) }, null, null, null);
		if (cursor.getCount() > 0) {
			cursor.moveToNext();
		}
		return this.cursorToContact(cursor);
	}

	public int updateContact(ContactContract contact) {

		ContentValues values = new ContentValues();

		values.put(ContactDbHelper.COL_FIRST_NAME, contact.getFirstName());
		values.put(ContactDbHelper.COL_LAST_NAME, contact.getLastName());
		values.put(ContactDbHelper.COL_ADDRESS, contact.getAddress());
		values.put(ContactDbHelper.COL_EMAIL_ADDRESS, contact.getEmailAddress());
		values.put(ContactDbHelper.COL_NUMBER, contact.getNumber());
		values.put(ContactDbHelper.COL_HAS_IMAGE, contact.hasImage());

		return this.db.update(ContactDbHelper.CONTACT_TABLE_NAME, values, ContactDbHelper.COL_ID + "=" + contact.getID(), null);
	}

	public boolean removeContact(int contactID) {
		return db.delete(ContactDbHelper.CONTACT_TABLE_NAME, ContactDbHelper.COL_ID+ "=" + contactID, null) > 0;
	}

	private ContactContract cursorToContact(Cursor c){
		if (c.getCount() == 0) {
			return null;
		}

		ContactContract contactContract = new ContactContract();
		contactContract.setID(c.getInt(0));
		contactContract.setFirstName(c.getString(1));
		contactContract.setLastName(c.getString(2));
		contactContract.setAddress(c.getString(3));
		contactContract.setEmailAddress(c.getString(4));
		contactContract.setNumber(c.getString(5));
		contactContract.setImage(c.getInt(6) == 1);
		return contactContract;
	}
}
