package com.theogros.ft_hangout.controllers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by theog on 19/05/2018.
 */

public class SettingsDbHelper extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 2;
	private static final String DATABASE_NAME = "ContactContract.db";



	protected static final String SETTINGS_TABLE_NAME = "settings";
	protected static final String COL_ID = "ID";
	protected static final String COL_SETTING_NAME = "setting";
	protected static final String COL_SETTING_VALUE = "value";

	public static final String ROW_COLOR_THEME = "colorTheme";

	private static final String SETTINGS_TABLE_CREATE =
			"CREATE TABLE IF NOT EXISTS " + SETTINGS_TABLE_NAME + " ("
					+ COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
					+ SettingsDbHelper.COL_SETTING_NAME + " TEXT NOT NULL, "
					+ SettingsDbHelper.COL_SETTING_VALUE + " TEXT NOT NULL);";

	private static final String SETTINGS_INSERT_DEFAULT_COLOR_THEME =
			"INSERT OR IGNORE INTO " + SETTINGS_TABLE_NAME + "(" +
					COL_ID + ", " + COL_SETTING_NAME + ", " + COL_SETTING_VALUE +
					") VALUES (1, \"colorTheme\", \"1\");";

	SettingsDbHelper(Context context) {
		super (context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SettingsDbHelper.SETTINGS_TABLE_CREATE);
		db.execSQL(SettingsDbHelper.SETTINGS_INSERT_DEFAULT_COLOR_THEME);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + SettingsDbHelper.SETTINGS_TABLE_NAME + ";");
		this.onCreate(db);
	}
}
