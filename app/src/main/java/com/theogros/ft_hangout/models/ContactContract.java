package com.theogros.ft_hangout.models;

import java.io.Serializable;

/**
 * Created by theog on 19/05/2018.
 */

public class ContactContract implements Serializable {

	private int ID;
	private String firstName;
	private String lastName;
	private String number;
	private String address;
	private String emailAddress;
	private boolean image;

	public ContactContract() {}

	public int getID() {
		return this.ID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getNumber() {
		return number;
	}

	public void setID(int id) {
		this.ID = id;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public boolean hasImage() {
		return image;
	}

	public void setImage(boolean hasImage) {
		this.image = hasImage;
	}

	public String getFullName() {
		String fullName = "";

		if (this.firstName != null && !this.firstName.isEmpty()) {
			fullName = this.firstName;
		}
		if (this.lastName != null && !this.lastName.isEmpty()) {
			fullName += " " + this.lastName;
		}
		return fullName;
	}
}
