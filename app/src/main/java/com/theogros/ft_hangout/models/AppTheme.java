package com.theogros.ft_hangout.models;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.view.Window;
import android.view.WindowManager;

public class AppTheme {

	private static int colorPrimary = 0x3F51B5;
	private static int colorPrimaryDark = 0x303F9F;

	public static void setColorPrimary(int color) {
		colorPrimary = color;
	}

	public static void setColorPrimaryDark(int color) {
		colorPrimaryDark = color;
	}

	public static int getColorPrimary() {
		return colorPrimary;
	}

	public static int getColorPrimaryDark() {
		return colorPrimaryDark;
	}

	public static void setTheme(ActionBar actionBar, Activity activity) {

		int color = AppTheme.getColorPrimary();
		actionBar.setBackgroundDrawable(new ColorDrawable(Color.rgb( Color.red(color), Color.green(color), Color.blue(color)) ) );

		Window window = activity.getWindow();

		window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
		window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
		window.setStatusBarColor(AppTheme.getColorPrimaryDark());
	}

	public static void updateColorFromThemeID(int themeID, Activity activity) {
		String colorID = "colorTheme" + themeID;
		String darkColorID = "colorThemeDark" + themeID;

		final int colorResID = activity.getResources().getIdentifier(colorID, "color", activity.getPackageName());
		final int darkColorResID = activity.getResources().getIdentifier(darkColorID, "color", activity.getPackageName());

		AppTheme.colorPrimary = ContextCompat.getColor(activity, colorResID);
		AppTheme.colorPrimaryDark = ContextCompat.getColor(activity, darkColorResID);
	}
}
