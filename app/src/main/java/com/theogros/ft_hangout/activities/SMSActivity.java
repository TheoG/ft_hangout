package com.theogros.ft_hangout.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.theogros.ft_hangout.R;
import com.theogros.ft_hangout.controllers.SmsReceiver;
import com.theogros.ft_hangout.models.AppTheme;
import com.theogros.ft_hangout.models.ContactContract;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class SMSActivity extends AppCompatActivity {

	ContactContract currentContact = null;
	final ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
	ListView smsListView;
	SimpleAdapter simpleAdapter;
	SmsReceiver smsReceiver = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sms);



		ActionBar ab = getSupportActionBar();

		if (ab != null) {
			AppTheme.setTheme(ab, this);
			ab.setDisplayHomeAsUpEnabled(true);
			ab.setDisplayShowHomeEnabled(true);
		}

		if (checkCallingOrSelfPermission(Manifest.permission.READ_SMS) == PackageManager.PERMISSION_DENIED) {
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS, Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS}, 1);
			Toast.makeText(this, R.string.error_sms_permission_denied, Toast.LENGTH_SHORT).show();
			finish();
			return;
		}

		this.smsListView = findViewById(R.id.sms_list_view);

		this.smsListView.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);

		final Intent currentIntent = this.getIntent();
		Bundle extras = currentIntent.getExtras();

		if (extras != null) {

			this.currentContact = (ContactContract)extras.get("com.theogros.ft_hangout.activities.contact");
			ImageButton b = findViewById(R.id.action_send_sms);

			getSupportActionBar().setTitle(this.currentContact.getFullName());
			this.retrieveSms();

			b.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {

					EditText editTextSMS = findViewById(R.id.sms_body);
					String smsBody = editTextSMS.getText().toString();
					if (smsBody.isEmpty()) {
						return ;
					}

					SmsManager smsManager = SmsManager.getDefault();
					smsManager.sendTextMessage(currentContact.getNumber(), null, smsBody, null, null);

					SimpleDateFormat sdf = new SimpleDateFormat("E d MMM - HH:mm", getResources().getConfiguration().getLocales().get(0));
					Date date = new Date();

					HashMap<String, String> hashMap = new HashMap<>();
					hashMap.put("body", smsBody);
					hashMap.put("longDate", sdf.format(date));
					hashMap.put("type", Integer.toString(Telephony.TextBasedSmsColumns.MESSAGE_TYPE_SENT));
					arrayList.add(hashMap);

					if (simpleAdapter != null) {
						simpleAdapter.notifyDataSetChanged();
						smsListView.setSelection(simpleAdapter.getCount() - 1);
					}

					editTextSMS.getText().clear();
				}
			});




		} else {
			// Toast ?
			finish();
		}

		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
		intentFilter.setPriority(100);

		smsReceiver = new SmsReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				super.onReceive(context, intent);
				arrayList.clear();
				retrieveSms();
			}
		};
		registerReceiver(smsReceiver, intentFilter);
	}

	void retrieveSms() {

		final String SMS_URI_ALL = "content://sms/";
		try {
			Uri uri = Uri.parse(SMS_URI_ALL);
			String[] projection = new String[] { "_id", "address", "person", "body", "date", "type" };
			Cursor cur = getContentResolver().query(uri, projection, "address='"+ this.currentContact.getNumber() +"'", null, "date asc");

			if (cur != null && cur.moveToFirst()) {
				int index_Body = cur.getColumnIndex("body");
				int index_Date = cur.getColumnIndex("date");
				int index_Type = cur.getColumnIndex("type");


				do {
					String strbody = cur.getString(index_Body);
					long longDate = cur.getLong(index_Date);
					int int_Type = cur.getInt(index_Type);

					SimpleDateFormat sdf = new SimpleDateFormat("E d MMM - HH:mm", getResources().getConfiguration().getLocales().get(0));
					Date date = new Date(longDate);

					HashMap<String, String> hashMap = new HashMap<>();
					hashMap.put("body", strbody);
					hashMap.put("longDate", sdf.format(date));
					hashMap.put("type", Integer.toString(int_Type));
					arrayList.add(hashMap);

				} while (cur.moveToNext());

				if (!cur.isClosed()) {
					cur.close();
				}


			}

			String[] from = {"body", "longDate"};
			int[] to = {R.id.sent_sms_body, R.id.sms_date};

			final int contactColor = HomeActivity.stringToColor(currentContact.getFullName());
			final int whiteColor = getResources().getColor(android.R.color.white, null);
			final int blackColor = getResources().getColor(android.R.color.black, null);

			simpleAdapter = new SimpleAdapter(this, arrayList, R.layout.list_view_sms, from, to) {
				@Override
				public View getView(int position, View convertView, ViewGroup parent) {
					View view = super.getView(position, convertView, parent);

					TextView bodyMessage = view.findViewById(R.id.sent_sms_body);

					TextView leftPadding = view.findViewById(R.id.left_empty_text_view);
					TextView rightPadding = view.findViewById(R.id.right_empty_text_view);
					TextView smsDate = view.findViewById(R.id.sms_date);

					if (Integer.parseInt(arrayList.get(position).get("type")) == Telephony.TextBasedSmsColumns.MESSAGE_TYPE_INBOX) {
						bodyMessage.setBackgroundResource(R.drawable.speech_bubble_left);
						GradientDrawable shape = (GradientDrawable) bodyMessage.getBackground();
						shape.setColor(contactColor);
						bodyMessage.setTextColor(whiteColor);

						LinearLayout.LayoutParams bigLayoutParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, 10.0f);
						LinearLayout.LayoutParams smallLayoutParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, 5.0f);

						leftPadding.setLayoutParams(bigLayoutParam);
						rightPadding.setLayoutParams(smallLayoutParam);
						smsDate.setGravity(Gravity.START);
					} else {
						RelativeLayout relativeLayout = view.findViewById(R.id.sent_sms_relative_layout);
						relativeLayout.setGravity(Gravity.END);
						bodyMessage.setTextColor(blackColor);
						bodyMessage.setBackgroundResource(R.drawable.speech_bubble_right);

						LinearLayout.LayoutParams bigLayoutParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, 10.0f);
						LinearLayout.LayoutParams smallLayoutParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, 9.0f);

						leftPadding.setLayoutParams(smallLayoutParam);
						rightPadding.setLayoutParams(bigLayoutParam);
						smsDate.setGravity(Gravity.END);
					}

					return view;

				}
			};



			smsListView.setAdapter(simpleAdapter);
			smsListView.setSelection(simpleAdapter.getCount() - 1);
		} catch (SQLiteException ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				finish();
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (this.smsReceiver != null) {
			unregisterReceiver(this.smsReceiver);
		}
	}
}
