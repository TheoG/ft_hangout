package com.theogros.ft_hangout.activities;

import android.app.Activity;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.theogros.ft_hangout.R;
import com.theogros.ft_hangout.controllers.SettingsController;
import com.theogros.ft_hangout.controllers.SettingsDbHelper;
import com.theogros.ft_hangout.models.AppTheme;

public class SettingsActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_settings);

		final ActionBar ab = getSupportActionBar();

		for (int i = 1; i < 13; ++i) {
			String buttonID = "settings_color_button_" + i;
			String colorID = "colorTheme" + i;
			String darkColorID = "colorThemeDark" + i;

			int buttonResID = getResources().getIdentifier(buttonID, "id", getPackageName());
			final int colorResID = getResources().getIdentifier(colorID, "color", getPackageName());
			final int darkColorResID = getResources().getIdentifier(darkColorID, "color", getPackageName());

			Button b = findViewById(buttonResID);
			final int color = ContextCompat.getColor(this, colorResID);
			final int darkColor = ContextCompat.getColor(this, darkColorResID);
			final Activity activity = this;

			final int themeID = i;

			b.setBackgroundTintList(ColorStateList.valueOf(color));

			b.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					SettingsController settingsController = new SettingsController(getApplicationContext());
					settingsController.updateSetting(SettingsDbHelper.ROW_COLOR_THEME, Integer.toString(themeID));
					AppTheme.setColorPrimary(color);
					AppTheme.setColorPrimaryDark(darkColor);
					if (ab != null)
						AppTheme.setTheme(ab, activity);
					settingsController.close();
				}
			});
		}
		if (ab != null) {
			AppTheme.setTheme(ab, this);
			ab.setDisplayHomeAsUpEnabled(true);
			ab.setDisplayShowHomeEnabled(true);
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_settings, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				finish();
				break;
		}

		return super.onOptionsItemSelected(item);
	}

}
