package com.theogros.ft_hangout.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.icu.util.Calendar;
import android.icu.util.TimeZone;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.theogros.ft_hangout.R;
import com.theogros.ft_hangout.controllers.ContactController;
import com.theogros.ft_hangout.controllers.SettingsController;
import com.theogros.ft_hangout.controllers.SettingsDbHelper;
import com.theogros.ft_hangout.controllers.SmsReceiver;
import com.theogros.ft_hangout.models.AppTheme;
import com.theogros.ft_hangout.models.ContactContract;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


public class HomeActivity extends AppCompatActivity {

	private boolean onResumeFromBackground = false;
	private boolean onResumeFromActivity = false;
	private boolean shouldDisplayToast;
	private String onResumeString;

	private SmsReceiver smsReceiver = null;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		Toolbar toolbar = findViewById(R.id.contact_information_toolbar);
		setSupportActionBar(toolbar);
		this.shouldDisplayToast = false;

		SettingsController settingsController = new SettingsController(getApplicationContext());
		String colorThemeIDStr = settingsController.getSettingValue(SettingsDbHelper.ROW_COLOR_THEME);
		settingsController.close();

		AppTheme.updateColorFromThemeID(Integer.parseInt(colorThemeIDStr), this);
		ActionBar ab = getSupportActionBar();
		if (ab != null) {
			AppTheme.setTheme(ab, this);
		}

		FloatingActionButton fab = findViewById(R.id.action_create_contact);
		fab.setBackgroundTintList(ColorStateList.valueOf(AppTheme.getColorPrimary()));

		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(view.getContext(), CreateContactActivity.class);
				intent.putExtra("com.theogros.ft_hangout.activities.type", "CREATE");
				onResumeFromActivity = true;
				startActivity(intent);
			}
		});

		if (checkCallingOrSelfPermission(Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED ||
				checkCallingOrSelfPermission(Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED ||
				checkCallingOrSelfPermission(Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS, Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS}, 1);
		}

		this.populateList();

		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
		intentFilter.setPriority(100);

		smsReceiver = new SmsReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				super.onReceive(context, intent);
				populateList();
			}
		};
		registerReceiver(smsReceiver, intentFilter);
	}

	@Override
	public void onPause() {
		super.onPause();
		this.onResumeFromBackground = true;

		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+1:00"));
		SimpleDateFormat df1 = new SimpleDateFormat("dd/MM/YY kk:mm:ss", getResources().getConfiguration().getLocales().get(0));
		this.onResumeString = df1.format(cal.getTime());
	}

	@Override
	public void onResume() {
		super.onResume();
		ActionBar ab = getSupportActionBar();
		if (ab != null) {
			AppTheme.setTheme(getSupportActionBar(), this);
		}
		findViewById(R.id.action_create_contact).setBackgroundTintList(ColorStateList.valueOf(AppTheme.getColorPrimary()));

		if (this.shouldDisplayToast && this.onResumeFromBackground && !this.onResumeFromActivity) {
			String text = getString(R.string.background_resume) + " " + this.onResumeString;
			Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
			this.onResumeFromBackground = false;
		}
		this.onResumeFromActivity = false;
		this.shouldDisplayToast = true;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (this.smsReceiver != null) {
			unregisterReceiver(this.smsReceiver);
		}
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		this.populateList();
	}

	private void populateList() {
		ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
		ContactController contactController = new ContactController(getApplicationContext());
		final ArrayList<ContactContract> contactContracts = contactController.getAllContacts();
		contactController.close();

		for (ContactContract c : contactContracts) {
			HashMap<String, String> hashMap = new HashMap<>();
			hashMap.put("id", Integer.toString(c.getID()));
			hashMap.put("name", c.getFullName());
			hashMap.put("num", c.getNumber());
			hashMap.put("init", c.getFirstName().charAt(0) + (c.getLastName() != null && !c.getLastName().isEmpty() ? c.getLastName().charAt(0) + "" : ""));
			hashMap.put("isPhoto", Boolean.toString(c.hasImage()));

			arrayList.add(hashMap);
		}


		String[] from = {"name", "num", "init"};
		int[] to = {R.id.contactName, R.id.contactNum, R.id.contactImage};

		ListView contactListView = findViewById(R.id.contactListView);

		SimpleAdapter simpleAdapter1 = new SimpleAdapter(this, arrayList, R.layout.list_view_contacts, from, to) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				View view = super.getView(position, convertView, parent);

				TextView contactImage = view.findViewById(R.id.contactImage);
				contactImage.setTextColor(Color.WHITE);

				// noinspection unchecked
				HashMap<String, String> rowData = (HashMap<String, String>) getItem(position);
				String name = rowData.get("name");

				contactImage.setBackgroundResource(R.drawable.rounded_corners);

				if (rowData.get("isPhoto").equals("true")) {
					contactImage.setText("");
					try {
						FileInputStream fileInputStream = openFileInput(ContactController.contactFileNamePattern + rowData.get("id") + ContactController.contactFileExtension);
						Drawable d = Drawable.createFromStream(fileInputStream, ContactController.contactFileNamePattern  + rowData.get("id") + ContactController.contactFileExtension);
						contactImage.setBackground(d);
					} catch (FileNotFoundException e) {
						Toast.makeText(getApplicationContext(), R.string.error_file_not_found, Toast.LENGTH_SHORT).show();
					}
				} else {
					GradientDrawable drawable = (GradientDrawable) contactImage.getBackground();
					drawable.setColor(stringToColor(name));
				}
				return view;

			}
		};

		contactListView.setAdapter(simpleAdapter1);

		contactListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				final ContactContract contactInformation = contactContracts.get(i);
				Intent intent = new Intent(view.getContext(), ContactInfoActivity.class);
				intent.putExtra("com.theogros.ft_hangout.activities.contact", contactInformation);
				onResumeFromActivity = true;
				startActivity(intent);
			}
		});
	}

	public static int stringToColor(String color) {
		List<Integer> list = new ArrayList<>(Arrays.asList(0xFFAB47BC, 0xFFEC407A, 0xFFEF5350, 0xFF7986CB, 0xFF1E88E5,
				0xFF9575CD, 0xFF2979FF, 0xFF009688, 0xFF0097A7, 0xFF0288D1, 0xFF558B2F,
				0xFF43A047, 0xFFE65100, 0xFFF4511E, 0xFFD84315, 0xFF8D6E63, 0xFF78909C));

		return list.get(Math.abs(color.hashCode()) % list.size());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_home, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if (id == R.id.action_settings) {
			Intent settingsIntent = new Intent(this, SettingsActivity.class);
			onResumeFromActivity = true;
			startActivity(settingsIntent);
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
		public void onRequestPermissionsResult(int requestCode, @Nullable String permissions[], @Nullable int[] grantResults) {

		this.shouldDisplayToast = false;

		if (grantResults == null)
			return ;

		switch (requestCode) {
			case 1:
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED)
					Toast.makeText(this, "Denied...", Toast.LENGTH_SHORT).show();
				break;
			case 2:
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED)
					Toast.makeText(this, "Denied...", Toast.LENGTH_SHORT).show();
				break;
			case 3:
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED)
					Toast.makeText(this, "Denied...", Toast.LENGTH_SHORT).show();
				break;
		}
	}
}
