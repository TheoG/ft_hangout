package com.theogros.ft_hangout.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.theogros.ft_hangout.R;
import com.theogros.ft_hangout.controllers.ContactController;
import com.theogros.ft_hangout.models.AppTheme;
import com.theogros.ft_hangout.models.ContactContract;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ContactInfoActivity extends AppCompatActivity {

	private ContactContract currentContact;
	private ContactController contactController;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contact_info);

		Intent intent = this.getIntent();

		this.contactController = new ContactController(getApplicationContext());
		final Activity activity = this;

		Bundle extras = intent.getExtras();
		if (extras != null) {

			this.currentContact = (ContactContract)extras.get("com.theogros.ft_hangout.activities.contact");
			if (this.currentContact != null) {
				this.fillInformation(this.currentContact);
			} else {
				finish();
				return ;
			}

			FloatingActionButton editContactActionButton = findViewById(R.id.action_edit_contact);
			editContactActionButton.setBackgroundTintList(ColorStateList.valueOf(AppTheme.getColorPrimary()));
			editContactActionButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					Intent intent = new Intent(view.getContext(), CreateContactActivity.class);
					intent.putExtra("com.theogros.ft_hangout.activities.type", "EDIT");
					intent.putExtra("com.theogros.ft_hangout.activities.contact", currentContact);
					startActivityForResult(intent, 1);
				}
			});

			FloatingActionButton writeSMS = findViewById(R.id.action_open_sms);
			writeSMS.setBackgroundTintList(ColorStateList.valueOf(AppTheme.getColorPrimary()));
			writeSMS.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					Intent intent = new Intent(view.getContext(), SMSActivity.class);
					intent.putExtra("com.theogros.ft_hangout.activities.contact", currentContact);
					startActivity(intent);
			}
			});

			FloatingActionButton callFab = findViewById(R.id.action_call_contact);
			callFab.setBackgroundTintList(ColorStateList.valueOf(AppTheme.getColorPrimary()));
			callFab.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {

					if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
						ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, 1);
					} else {
						Intent callIntent = new Intent(Intent.ACTION_CALL);
						callIntent.setData(Uri.parse("tel:" + currentContact.getNumber()));
						startActivity(callIntent);
					}
				}
			});
		}
		ActionBar ab = getSupportActionBar();
		if (ab != null) {
			AppTheme.setTheme(getSupportActionBar(), this);
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @Nullable String permissions[], @Nullable int[] grantResults) {

		if (grantResults == null) {
			return ;
		}

		switch (requestCode) {
		case 1:
			if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
				Toast.makeText(this, R.string.error_warning_user, Toast.LENGTH_SHORT).show();
			}
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 1) {
			if (resultCode == Activity.RESULT_OK) {
				this.currentContact = this.contactController.getContactByID(this.currentContact.getID());
				this.fillInformation(this.currentContact);
			}
		}
	}

	private void fillInformation(ContactContract contact) {
		TextView contactNumber = findViewById(R.id.contact_info_number);
		TextView contactAddress = findViewById(R.id.contact_info_address);
		TextView contactEmailAddress = findViewById(R.id.contact_info_email_address);

		contactNumber.setText(contact.getNumber());

		String contactAddressString = contact.getAddress();
		LinearLayout addressLayout = findViewById(R.id.linear_layout_address);
		if (contactAddressString == null || contactAddressString.isEmpty()) {
			addressLayout.setVisibility(View.GONE);
		}

		String contactEmailAddressString = contact.getEmailAddress();
		LinearLayout emailAddressLayout = findViewById(R.id.linear_layout_email);
		if (contactEmailAddressString == null || contactEmailAddressString.isEmpty()) {
			emailAddressLayout.setVisibility(View.GONE);
		}
		contactAddress.setText(contact.getAddress());
		contactEmailAddress.setText(contact.getEmailAddress());


		ImageView imageView = findViewById(R.id.app_bar_image);
		if (contact.hasImage()) {
			try {
				FileInputStream fileInputStream = openFileInput(ContactController.contactFileNamePattern + contact.getID() + ContactController.contactFileExtension);
				imageView.setImageBitmap(BitmapFactory.decodeStream(fileInputStream));
			} catch (FileNotFoundException e) {
				System.err.println(e.getMessage());
			}
		}


		ActionBar ab = getSupportActionBar();
		if (ab != null) {
			ab.setTitle(contact.getFullName());
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_contact_information, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if (id == R.id.action_remove_contact) {
			ContactController contactController = new ContactController(getApplicationContext());
			boolean success = contactController.removeContact(this.currentContact.getID());
			boolean isDeleted = true;

			if (!success) {
				Toast.makeText(getApplicationContext(), R.string.error_remove_contact, Toast.LENGTH_SHORT).show();
			} else {
				File dir = getFilesDir();
				File file = new File(dir, ContactController.contactFileNamePattern + this.currentContact.getID() + ContactController.contactFileExtension);
				isDeleted = file.delete();
			}
			contactController.close();
			finish();
			return isDeleted;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		this.contactController.close();
	}
}
