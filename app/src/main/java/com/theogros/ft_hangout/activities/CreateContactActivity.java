package com.theogros.ft_hangout.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.theogros.ft_hangout.R;
import com.theogros.ft_hangout.controllers.ContactController;
import com.theogros.ft_hangout.models.AppTheme;
import com.theogros.ft_hangout.models.ContactContract;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;

public class CreateContactActivity extends AppCompatActivity {

	enum Type {
		CREATE,
		EDIT
	}

	boolean doubleBackToExitPressedOnce = false;
	Type activityType;
	ContactContract currentContact;
	String imagePath = null;

	private final static float IMAGE_MAX_WIDTH = 500.0f;
	private final static float IMAGE_MAX_HEIGHT = 500.0f;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_contact_form);

		ActionBar ab = getSupportActionBar();
		if (ab != null) {
			ab.setDisplayShowTitleEnabled(false);
			ab.setDisplayHomeAsUpEnabled(true);
			ab.setDisplayShowHomeEnabled(true);
			AppTheme.setTheme(ab, this);
		}

		Intent intent = getIntent();
		this.activityType = intent.getStringExtra("com.theogros.ft_hangout.activities.type").equals("CREATE") ? Type.CREATE : Type.EDIT;
		if (this.activityType == Type.EDIT) {

			Bundle bundle = intent.getExtras();
			if (bundle != null) {
				this.currentContact = (ContactContract)bundle.get("com.theogros.ft_hangout.activities.contact");
			} else {
				finish();
				return ;
			}

			EditText firstNameEditText	= findViewById(R.id.create_contact_first_name);
			EditText lastNameEditText	= findViewById(R.id.create_contact_last_name);
			EditText numberEditText		= findViewById(R.id.create_contact_tel_number);
			EditText emailEditText		= findViewById(R.id.create_contact_email_address);
			EditText addressEditText	= findViewById(R.id.create_contact_address);

			firstNameEditText.setText(this.currentContact.getFirstName());
			lastNameEditText.setText(this.currentContact.getLastName());
			numberEditText.setText(this.currentContact.getNumber());
			emailEditText.setText(this.currentContact.getEmailAddress());
			addressEditText.setText(this.currentContact.getAddress());

		}

		if (checkCallingOrSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
		}

		ImageView image = findViewById(R.id.imageView6);
		image.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg) {
				Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				startActivityForResult(intent, 42);
			}
		});
	}

	public String getPath(Uri uri, Activity activity) {
		Cursor cursor = null;
		try {
			String[] projection = {MediaStore.MediaColumns.DATA};
			cursor = activity.getContentResolver().query(uri, projection, null, null, null);
			if (cursor != null && cursor.moveToFirst()) {
				int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
				return cursor.getString(column_index);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (cursor != null) {
			cursor.close();
		}
		return "";
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 42 && resultCode == Activity.RESULT_OK && null != data) {

			Uri selectedImageUri = data.getData();
			String tempPath = getPath(selectedImageUri, this);
			if (selectedImageUri != null) {
				String url = selectedImageUri.toString();
				if (url.startsWith("content://com.google.android.apps.photos.content")){
					this.imagePath = url;
				} else {
					this.imagePath = tempPath;
				}
			}

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_create_contact, menu);
		return true;
	}

	@Override
	public void onBackPressed() {
		this.pressAgainToLeave();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				this.pressAgainToLeave();
				break;
			case R.id.create_contact_action_save:
				ContactController contactController = new ContactController(getApplicationContext());

				final ContactContract contact = new ContactContract();
				contact.setFirstName(((EditText)findViewById(R.id.create_contact_first_name)).getText().toString().trim());
				contact.setLastName(((EditText)findViewById(R.id.create_contact_last_name)).getText().toString().trim());
				contact.setNumber(((EditText)findViewById(R.id.create_contact_tel_number)).getText().toString().trim());
				contact.setAddress(((EditText)findViewById(R.id.create_contact_address)).getText().toString().trim());
				contact.setEmailAddress(((EditText)findViewById(R.id.create_contact_email_address)).getText().toString().trim());
				if (this.activityType == Type.CREATE) {
					contact.setImage(this.imagePath != null);
				} else {
					contact.setImage(this.currentContact.hasImage() || this.imagePath != null);
				}

				if (contact.getFirstName().trim().isEmpty() || contact.getFirstName() == null) {
					Toast.makeText(this, R.string.error_empty_first_name, Toast.LENGTH_SHORT).show();
					return true;
				}
				if (contact.getNumber().trim().isEmpty() || contact.getNumber() == null) {
					Toast.makeText(this, R.string.error_empty_number, Toast.LENGTH_SHORT).show();
					return true;
				}

				if (this.activityType == Type.CREATE) {
					long newRowId = contactController.insertContact(contact);
					if (newRowId < 0) {
						Toast.makeText(this, R.string.error_contact_insertion, Toast.LENGTH_SHORT).show();
						finish();
					}

					if (this.imagePath != null) {
						this.saveImageInternalMemory(newRowId);
					}

				} else {
					contact.setID(this.currentContact.getID());
					long rowAffected = contactController.updateContact(contact);
					if (rowAffected == 0) {
						Toast.makeText(this, R.string.error_contact_insertion, Toast.LENGTH_SHORT).show();
						finish();
					}

					if (this.imagePath != null) {
						this.saveImageInternalMemory(this.currentContact.getID());
					}
					Intent returnIntent = new Intent();
					setResult(Activity.RESULT_OK, returnIntent);
				}

				contactController.close();
				finish();
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void saveImageInternalMemory(long rowID) {
		BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		Bitmap bitmap = null;
		if (this.imagePath.startsWith("content://com.google.android.apps.photos.content")) {
			try {
				InputStream is = getContentResolver().openInputStream(Uri.parse(this.imagePath));
				InputStream isTmp = getContentResolver().openInputStream(Uri.parse(this.imagePath));
				if (is != null) {

					BitmapFactory.Options options = new BitmapFactory.Options();

					options.inJustDecodeBounds = true;
					BitmapFactory.decodeStream(isTmp, null, options);
					int imageHeight = options.outHeight;
					int imageWidth = options.outWidth;

					int ratio = Math.max((int)Math.ceil(imageHeight / CreateContactActivity.IMAGE_MAX_HEIGHT), (int)Math.ceil(imageWidth / CreateContactActivity.IMAGE_MAX_WIDTH));

					options.inJustDecodeBounds = false;
					options.inSampleSize = ratio;
					bitmap = BitmapFactory.decodeStream(is, null, options);
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		} else {
			bitmap = BitmapFactory.decodeFile(this.imagePath, bmOptions);
		}

		try {
			if (bitmap != null) {
				final FileOutputStream fos = openFileOutput(ContactController.contactFileNamePattern + String.valueOf(rowID) + ContactController.contactFileExtension, Context.MODE_PRIVATE);
				bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
			}

		} catch (FileNotFoundException e) {
			Toast.makeText(this, R.string.error_file_not_found, Toast.LENGTH_SHORT).show();
			finish();
		}
	}

	private void pressAgainToLeave() {
		EditText editTextFirstName = findViewById(R.id.create_contact_first_name);
		EditText editTextLastName = findViewById(R.id.create_contact_last_name);
		EditText editTextTelNumber = findViewById(R.id.create_contact_tel_number);
		EditText editTextAddress = findViewById(R.id.create_contact_address);
		EditText editTextEmail = findViewById(R.id.create_contact_email_address);

		boolean canGoBack = true;

		if (!editTextFirstName.getText().toString().trim().isEmpty())
			canGoBack = false;
		if (!editTextLastName.getText().toString().trim().isEmpty())
			canGoBack = false;
		if (!editTextTelNumber.getText().toString().trim().isEmpty())
			canGoBack = false;
		if (!editTextAddress.getText().toString().trim().isEmpty())
			canGoBack = false;
		if (!editTextEmail.getText().toString().trim().isEmpty())
			canGoBack = false;

		if (canGoBack) {
			finish();
			return;
		}

		if (this.doubleBackToExitPressedOnce) {
			finish();
		} else {
			this.doubleBackToExitPressedOnce = true;
			Toast.makeText(this, R.string.action_leave_confirmation, Toast.LENGTH_SHORT).show();
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					doubleBackToExitPressedOnce = false;
				}
			}, 2000);
		}
	}
}
